# meltano-superset-postgres-bundle-charm

this bundle charm instantiates a full suite of data tools to get started.  Meltano (ELT) , Postgresql (DB), Apache Superset (viz)

# How-To 

This Readme assumes you're already somewhat familiar with juju.. but if not basically you need to find a cloud.. so far the safest way to test this is in a local cloud like LXD, VMware, or MaaS (Metal As A Service)

if you have juju already setup just

```
juju deploy ./config.yaml
```

once that completes you can watch the `juju status` in order to grab what IPs your meltano installation went to.

```
$ juju status
Model        Controller           Cloud/Region         Version  SLA          Timestamp
meltano-dev  localhost-localhost  localhost/localhost  2.9.2    unsupported  21:51:47+02:00

App              Version  Status  Scale  Charm            Store     Channel  Rev  OS      Message
apache-superset  1.3.1    active      1  apache-superset  charmhub  beta       7  ubuntu  (update) Apache Superset listening on 8088: 21:49
meltano                   active      1  meltano          charmhub  beta       2  ubuntu  Meltano 21:48
postgresql       12.8     active      1  postgresql       charmhub  stable   235  ubuntu  Live master (12.8)

Unit                Workload  Agent  Machine  Public address  Ports     Message
apache-superset/0*  active    idle   2        1.2.3.57    8088/tcp      (update) Apache Superset listening on 8088: 21:49
meltano/2*          active    idle   4        1.2.3.76    5000/tcp      Meltano 21:48
postgresql/0*       active    idle   1        1.2.3.38    5432/tcp      Live master (12.8)

Machine  State    DNS           Inst id        Series  AZ  Message
1        started  1.2.3.38      juju-cccbb1-1  focal       Running
2        started  1.2.3.57      juju-cccbb1-2  focal       Running
4        started  1.2.3.76      juju-cccbb1-4  focal       Running

Relation provider       Requirer                Interface    Type     Message
postgresql:coordinator  postgresql:coordinator  coordinator  peer     
postgresql:db           apache-superset:db      pgsql        regular  
postgresql:db           meltano:db              pgsql        regular  
postgresql:replication  postgresql:replication  pgpeer       peer     


```

So in the case of the above, you'd go to meltano by pointing a browser at `1.2.3.76:5000` 

for apache superset, this charm has a default admin password combo `admin/09876` it is for obvious dev purposes only